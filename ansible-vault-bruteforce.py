import os
import sys

# Check number of arguments
if len(sys.argv) <= 2:
    print(""" Usage of this script :
    python3 ansible-vault-bruteforce.py <password list> <vault to decrypt>
    """)
    exit()
else:
    print("""
 █████╗ ███╗   ██╗███████╗██╗██████╗ ██╗     ███████╗    ██╗   ██╗ █████╗ ██╗   ██╗██╗     ████████╗    ██████╗ ██████╗ ██╗   ██╗████████╗███████╗███████╗ ██████╗ ██████╗  ██████╗███████╗
██╔══██╗████╗  ██║██╔════╝██║██╔══██╗██║     ██╔════╝    ██║   ██║██╔══██╗██║   ██║██║     ╚══██╔══╝    ██╔══██╗██╔══██╗██║   ██║╚══██╔══╝██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔════╝
███████║██╔██╗ ██║███████╗██║██████╔╝██║     █████╗      ██║   ██║███████║██║   ██║██║        ██║       ██████╔╝██████╔╝██║   ██║   ██║   █████╗  █████╗  ██║   ██║██████╔╝██║     █████╗  
██╔══██║██║╚██╗██║╚════██║██║██╔══██╗██║     ██╔══╝      ╚██╗ ██╔╝██╔══██║██║   ██║██║        ██║       ██╔══██╗██╔══██╗██║   ██║   ██║   ██╔══╝  ██╔══╝  ██║   ██║██╔══██╗██║     ██╔══╝  
██║  ██║██║ ╚████║███████║██║██████╔╝███████╗███████╗     ╚████╔╝ ██║  ██║╚██████╔╝███████╗   ██║       ██████╔╝██║  ██║╚██████╔╝   ██║   ███████╗██║     ╚██████╔╝██║  ██║╚██████╗███████╗
╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚═╝╚═════╝ ╚══════╝╚══════╝      ╚═══╝  ╚═╝  ╚═╝ ╚═════╝ ╚══════╝   ╚═╝       ╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚══════╝

    by gribHB""")
    print("")

# Variables to get password wordlist and the vault file which would to be decrypted
password_list = sys.argv[1]
vault = sys.argv[2]

# Count the number of password in the wordlist
cnt = len(list(open(password_list, "rb")))

print("There are total", cnt, "number of passwords to test.")

# Open password list and remove the line return
file_pass = open(password_list, encoding="latin-1")
mypassword_list = [line.rstrip('\n') for line in file_pass]

# For loop to testing decrypt the vault file with all password present in the wordlist
for word in mypassword_list:
    ansible_cmd = "echo '{}' > vaultpass.txt && ansible-vault decrypt {} --vault-password-file vaultpass.txt".format(word,vault)
    result = os.system(ansible_cmd)

    if result == 0:
        print("")
        print("FILE DECRYPTED, THE PASSWORD IS :", word)
        exit()

os.system('rm -rf vaultpass.txt')

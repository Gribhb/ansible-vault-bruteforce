# Ansibe Vault Bruteforce

A python script to brute force gaining access to an ansible vault.

## Usage

```bash
python3 ansible-vault-bruteforce.py <password list> <vault to decrypt>
```

And then if the vault is decrypted you cat just **cat** le vault file to see what's inside

Enjoy ! :)
